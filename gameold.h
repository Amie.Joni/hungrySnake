#ifndef DEF_GAME
#define DEF_GAME


void game(SDL_Surface *screen);
void init(int map[][NBLOC_HIGHT]);
bool orientSnake(int map[][NBLOC_HIGHT], int direction);
void targetPGenerate(int map[][NBLOC_HIGHT]);
bool moveField(int map[][NBLOC_HIGHT],int i,int j,int direction);
#endif // DEF_GAME
