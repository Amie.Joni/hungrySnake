// -----------------------------------------------------------------------------
// Here you will find general declaration of all files
// -----------------------------------------------------------------------------

#ifndef DEF_MAIN
#define DEF_MAIN
//screen parameters
#define BLOC_WH 20
#define NBLOC_WIDTH 30
#define NBLOC_HIGHT 20
#define SCREEN_WIDTH NBLOC_WIDTH * BLOC_WH
#define SCREEN_HIEGHT NBLOC_HIGHT * BLOC_WH

//snake direction
enum {
    UP,
    RIGHT,
    DOWN,
    LEFT
};

//map filling
enum {
    EMPTY,
    SNAKE,
    SNAKEHEAD,
    SNAKETAIL,
    TARGET,
    BARRIER
};

#endif // DEF_MAIN
