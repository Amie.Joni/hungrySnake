// -----------------------------------------------------------------------------
// In this project i have rellize a snake game using C + SDL Library
// it works fine test on linux mint
// compile :gcc -o snake.out main.c game.c -lSDL -lSDL_image -lSDL_ttf
// run : ./snake.out
// -----------------------------------------------------------------------------

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include "common.h"
#include "game.h"

int main ( int argc, char** argv )
{
    // declaration
    SDL_Surface *screen, *menuBeginning;
    SDL_Event event;
    SDL_Rect positionMenu;
    bool done = false;

    // initialize SDL video
    SDL_Init( SDL_INIT_VIDEO | SDL_DOUBLEBUF );

    // create a new surface
    screen  = SDL_SetVideoMode(SCREEN_WIDTH , SCREEN_HIEGHT, 32,SDL_HWSURFACE|SDL_DOUBLEBUF);

    // window title
    SDL_WM_SetCaption("SnakeHungry", NULL);

    // filling screen with a color
    SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 255, 255, 255));

    //load image
    menuBeginning = IMG_Load("imgs/beginning.jpg");
    positionMenu.x = 0;
    positionMenu.y = 0;

    // program main loop
    while (!done)
    {
        // message processing loop
        SDL_WaitEvent(&event);

        // check for messages
        switch (event.type)
        {
            // exit if the window is closed
            case SDL_QUIT:
                done = true;
                break;

            // check for keypresses
            case SDL_KEYDOWN:
                // action to do in each key
                switch(event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                    case SDLK_q:
                        done = true;
                        break;
                    case SDLK_s:
                        game(screen);
                        break;

                }
        } // end switch

        // clear screen
        SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 255, 255, 255));

        // draw menu
        SDL_BlitSurface(menuBeginning, NULL, screen, &positionMenu);

        // finally, update the screen :)
        SDL_Flip(screen);
    } // end main loop

    // free memory
    SDL_Quit();

    return EXIT_SUCCESS;
}
