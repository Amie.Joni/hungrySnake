// -----------------------------------------------------------------------------
// Here function implementation of game.h
// the main work is done in void game() function
// -----------------------------------------------------------------------------

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>

#include "common.h"
#include "game.h"

//speed value
int speed = 500;

//this is the main fonction of the game
void game(SDL_Surface *screen){

  SDL_Surface *snake = NULL, *target = NULL, *barrier = NULL,*gameover = NULL,*text = NULL;
  SDL_Rect position,positionText;
  SDL_Event event;
  bool done = false,gover=false;
  int i,j,tp=0,tc=0,directionRemembre = RIGHT,map[NBLOC_WIDTH][NBLOC_HIGHT] = {EMPTY};
  snakeCoo snakecoo[BLOC_WH*BLOC_WH];
  TTF_Font *font = NULL;
  SDL_Color fontColor = {0, 0, 255};
  char score[10];
  //text init
  TTF_Init();

  //fond load
  font = TTF_OpenFont("angelina.ttf", 65);

  //style
  TTF_SetFontStyle(font, TTF_STYLE_ITALIC);



  //  init rand
  srand(time(NULL));

  //  components creation
  snake   = SDL_CreateRGBSurface(SDL_HWSURFACE,BLOC_WH,BLOC_WH,32,0,0,0,0);
  target  = SDL_CreateRGBSurface(SDL_HWSURFACE,BLOC_WH,BLOC_WH,32,0,0,0,0);
  barrier  = SDL_CreateRGBSurface(SDL_HWSURFACE,BLOC_WH,BLOC_WH,32,0,0,0,0);
  gameover =  IMG_Load("imgs/end.jpg");

  //  components fill with color
  SDL_FillRect(snake, NULL, SDL_MapRGB(snake->format, 0, 255, 0));
  SDL_FillRect(target, NULL, SDL_MapRGB(target->format, 0, 0, 255));
  SDL_FillRect(barrier, NULL, SDL_MapRGB(barrier->format, 0, 0, 0));

  init(map,snakecoo);

  // main loop, will repeat till quit
  while (!done)
  {
    SDL_PollEvent(&event);
    switch(event.type)
    {
      case SDL_QUIT:
      done = true;
      break;
      case SDL_KEYDOWN:
      switch(event.key.keysym.sym)
      {
        case SDLK_ESCAPE:
        case SDLK_q:
        done = true;
        break;
        case SDLK_s:
        gover=false;
        directionRemembre = RIGHT;
        init(map,snakecoo);
        case SDLK_UP:
        directionRemembre = UP;
        break;
        case SDLK_DOWN:
        directionRemembre = DOWN;
        break;
        case SDLK_RIGHT:
        directionRemembre = RIGHT;
        break;
        case SDLK_LEFT:
        directionRemembre = LEFT;
        break;
      }
      break;
    }

    //text actual score
    sprintf(score, "%d", snakeLength(snakecoo)-2);
    text = TTF_RenderText_Blended(font,score, fontColor);

    // snake movement
    tc = SDL_GetTicks();
    if (tc - tp > speed)
    {
      //will move the snake every speed time periodically
      gover = !moveSnake(map,snakecoo,directionRemembre);
      tp = tc;
    }else{
      SDL_Delay(speed - (tc-tp));
    }

    //clean screen
    SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 255, 255, 255));
    if(!gover){ //draw map with components
      for(i=0;i<NBLOC_WIDTH;i++){
        for(j=0;j<NBLOC_HIGHT;j++){
          position.x = i*BLOC_WH;
          position.y = j*BLOC_WH;

          switch(map[i][j])
          {
            case SNAKE:
            case SNAKEHEAD:
            case SNAKETAIL:
            SDL_BlitSurface(snake, NULL, screen, &position);
            break;
            case TARGET:
            SDL_BlitSurface(target, NULL, screen, &position);
            break;
            case BARRIER:
            SDL_BlitSurface(barrier, NULL, screen, &position);
            break;
          }
        }
      }
    }else{// game over screen
      position.x = 0;
      position.y = 0;
      positionText.x = screen->w/2;
      positionText.y = screen->h/2;
      SDL_BlitSurface(gameover, NULL, screen, &position);
      SDL_BlitSurface(text, NULL, screen, &positionText);
    }
    //update screen
    SDL_Flip(screen);
  }
  // clear memory
  SDL_FreeSurface(snake);
  SDL_FreeSurface(target);
  SDL_FreeSurface(barrier);
  SDL_Quit();
}

// Init the map of the game
void init(int map[][NBLOC_HIGHT],snakeCoo snakecoo[]){
  int i,j;
  for(i=0;i<NBLOC_WIDTH;i++){
    for(j=0;j<NBLOC_HIGHT;j++){
      // barrier in the first and the last (ligne and conlumn)
      if(i == 0 || i == NBLOC_WIDTH-1 || j == 0 || j == NBLOC_HIGHT-1){
        map[i][j] = BARRIER;
        // snake head & tail position
      }else if(i == NBLOC_WIDTH/2 && j==NBLOC_HIGHT/2){
        snakecoo[0].type = SNAKETAIL;
        snakecoo[0].x = i-1;
        snakecoo[0].y = j;

        snakecoo[1].type = SNAKEHEAD;
        snakecoo[1].x = i;
        snakecoo[1].y = j;
        // Empty field
      }else map[i][j] = EMPTY;
    }
  }
  map[snakecoo[0].x][snakecoo[0].y] = snakecoo[0].type;
  map[snakecoo[1].x][snakecoo[1].y] = snakecoo[1].type;
  //target position
  targetPGenerate(map);
}

//clean old snake from map
void clearMap(int map[][NBLOC_HIGHT]){
  int i,j;
  for(i=0;i<NBLOC_WIDTH;i++)
  for(j=0;j<NBLOC_HIGHT;j++)
  if( map[i][j] != BARRIER && map[i][j] != TARGET) map[i][j] = EMPTY;
}

// Place target to a random position
void targetPGenerate(int map[NBLOC_WIDTH][NBLOC_HIGHT]){
  int i,j;
  bool found =false;
  for(i=rand()%(NBLOC_WIDTH-1);found==false;i++)
  for(j=rand()%(NBLOC_WIDTH-1);found==false;j++)
  if(map[i][j] == EMPTY){
    map[i][j] = TARGET;
    found=true;
  }
}

//moving the whole snake to a direction
bool moveSnake(int map[][NBLOC_HIGHT],snakeCoo snakecoo[], int direction){
  int i;

  //shift all positions
  for(i=0;i<snakeLength(snakecoo) - 1;i++){
    snakecoo[i].x = snakecoo[i+1].x;
    snakecoo[i].y = snakecoo[i+1].y;
  }

  //change head position & increase length if target
  switch(direction){
    case UP:
      switch(map[snakecoo[snakeLength(snakecoo) - 1].x][snakecoo[snakeLength(snakecoo) - 1].y-1]){
        case TARGET:
        map[snakecoo[snakeLength(snakecoo) - 1].x][snakecoo[snakeLength(snakecoo) - 1].y-1] = SNAKEHEAD;
        snakecoo[snakeLength(snakecoo)] = snakecoo[snakeLength(snakecoo) - 1];
        snakecoo[snakeLength(snakecoo)].y--;
        snakecoo[snakeLength(snakecoo) - 1].type=SNAKE;
        speed-=5;
        targetPGenerate(map);
        break;
        case SNAKE:
        case SNAKETAIL:
        case BARRIER:
        return false;
        break;
        default: snakecoo[snakeLength(snakecoo) - 1].y--;
      }
    break;
    case RIGHT:
      switch(map[snakecoo[snakeLength(snakecoo) - 1].x+1][snakecoo[snakeLength(snakecoo) - 1].y]){
        case TARGET:
        map[snakecoo[snakeLength(snakecoo) - 1].x+1][snakecoo[snakeLength(snakecoo) - 1].y] = SNAKEHEAD;
        snakecoo[snakeLength(snakecoo)] = snakecoo[snakeLength(snakecoo) - 1];
        snakecoo[snakeLength(snakecoo)].x++;
        snakecoo[snakeLength(snakecoo) - 1].type=SNAKE;
        speed-=5;
        targetPGenerate(map);
        break;
        case SNAKE:
        case SNAKETAIL:
        case BARRIER:
        return false;
        break;
        default: snakecoo[snakeLength(snakecoo) - 1].x++;
      }
    break;
    case DOWN:
      switch(map[snakecoo[snakeLength(snakecoo) - 1].x][snakecoo[snakeLength(snakecoo) - 1].y+1]){
        case TARGET:
        map[snakecoo[snakeLength(snakecoo) - 1].x][snakecoo[snakeLength(snakecoo) - 1].y+1] = SNAKEHEAD;
        snakecoo[snakeLength(snakecoo)] = snakecoo[snakeLength(snakecoo) - 1];
        snakecoo[snakeLength(snakecoo)].y++;
        snakecoo[snakeLength(snakecoo) - 1].type=SNAKE;
        speed-=5;
        targetPGenerate(map);
        break;
        case SNAKE:
        case SNAKETAIL:
        case BARRIER:
        return false;
        break;
        default: snakecoo[snakeLength(snakecoo) - 1].y++;
      }
    break;
    case LEFT:
      switch(map[snakecoo[snakeLength(snakecoo) - 1].x-1][snakecoo[snakeLength(snakecoo) - 1].y]){
        case TARGET:
        map[snakecoo[snakeLength(snakecoo) - 1].x+1][snakecoo[snakeLength(snakecoo) - 1].y] = SNAKEHEAD;
        snakecoo[snakeLength(snakecoo)] = snakecoo[snakeLength(snakecoo) - 1];
        snakecoo[snakeLength(snakecoo)].x--;
        snakecoo[snakeLength(snakecoo) - 1].type=SNAKE;
        speed-=5;
        targetPGenerate(map);
        break;
        case SNAKE:
        case SNAKETAIL:
        case BARRIER:
        return false;
        break;
        default: snakecoo[snakeLength(snakecoo) - 1].x--;
      }
    break;
  }
  //merge modif to map
  mergeSnake(map,snakecoo);
  //ok
  return true;
}

//merge snake array in map array
bool mergeSnake(int map[NBLOC_WIDTH][NBLOC_HIGHT],snakeCoo snakecoo[]){
  int i;
  clearMap(map);//clear map first except barriers and target
  for(i=0;i<snakeLength(snakecoo);i++){
    map[snakecoo[i].x][snakecoo[i].y] = snakecoo[i].type;
  }
  return true;
}

// return the snake length
int snakeLength(snakeCoo snakecoo[]){
  int i,c;
  for(i=0,c=0;snakecoo[i].type != SNAKEHEAD;i++) c++;
  return c+1;
}
