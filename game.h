// -----------------------------------------------------------------------------
// ^ Author: Amine JONAHI
// Here you will find declaration of struct and functions prototypes used in game.c
//
// -----------------------------------------------------------------------------

#ifndef DEF_GAME
#define DEF_GAME

typedef struct snakeCoo{
  int type;
  int x;
  int y;
} snakeCoo;

void game(SDL_Surface *screen);
void init(int map[NBLOC_WIDTH][NBLOC_HIGHT],snakeCoo snakecoo[]);
void targetPGenerate(int map[NBLOC_WIDTH][NBLOC_HIGHT]);
bool moveSnake(int map[][NBLOC_HIGHT],snakeCoo snakecoo[], int direction);
void clearMap(int map[][NBLOC_HIGHT]);
bool mergeSnake(int map[NBLOC_WIDTH][NBLOC_HIGHT],snakeCoo snakecoo[]);
int snakeLength(snakeCoo snakecoo[]);
#endif // DEF_GAME
