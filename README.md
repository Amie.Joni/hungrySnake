# hungrySnake
This is a project in where i have tried to implement snake game in c using SDL library.
Tf you have any suggestion or remarque don't hesitate to contact me.

## Prerequisites
you must have the SDL library installed with it's packages sdl_image and sdl_ttf
on linux systems you can simply do as follow :

https://gist.github.com/BoredBored/3187339a99f7786c25075d4d9c80fad5

### compile
```
gcc -o snake.out main.c game.c -lSDL -lSDL_image -lSDL_ttf
```
### run
```
./snake.out
```

![Alt text](https://github.com/AmineJonahi/hungrySnake/blob/master/Selection_006.png?raw=true "Start screen")
![Alt text](https://github.com/AmineJonahi/hungrySnake/blob/master/Selection_007.png?raw=true "Game screen")
![Alt text](https://github.com/AmineJonahi/hungrySnake/blob/master/Selection_008.png?raw=true "Gameover screen")
